package com.jtc.transpagoserver.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jtc.transpagoserver.bean.Cuenta;
import com.jtc.transpagoserver.bean.Usuario;

@Repository
public class CuentaRepositoryImpl implements CuentaRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Cuenta> obtenerCuentasUsuario(String nroDoc) {
		
		List<Cuenta> cuentas = jdbcTemplate.query("select * from cuentas c"				
				+ " join usuarios_cuentas uc on uc.idcuenta = c.idcuenta"
				+ " join usuarios u on u.idusuario = uc.idusuario"
				+ " where u.nrodocumento = ?", 
				new Object[] {nroDoc}, 
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setId(rs.getInt("idcuenta"));
						c.setNroCuenta(rs.getString("nrocuenta"));						
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuentas;
	}
	
	
	@Override
	public Cuenta obtenerCuenta(String nroCuenta) {
		
		Cuenta cuenta = jdbcTemplate.queryForObject("select * from cuentas c"
				+ " where c.nrocuenta = ?", 
				new Object[] {nroCuenta}, 
				(rs, rowNum) -> {					
					
						Cuenta c = new Cuenta();
						c.setId(rs.getInt("idcuenta"));
						c.setNroCuenta(rs.getString("nrocuenta"));						
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
					
					return c;
				});
		return cuenta;
	}
	
	@Override
	public Usuario obtenerUsuarioCuenta(String nroCuenta) {
		
		Usuario usuario = jdbcTemplate.queryForObject("select * from usuarios u"
				+ " join usuarios_cuentas uc on uc.idusuario = u.idusuario"
				+ " join cuentas c on c.idcuenta = uc.idcuenta"
				+ " where c.nrocuenta = ?", 
				new Object[] {nroCuenta}, 
				(rs, rowNum) -> {					
					
					Usuario u = new Usuario();
					u.setId(rs.getInt("idusuario"));
					u.setNroDoc(rs.getString("nrodocumento"));						
					u.setNombre(rs.getString("nombre"));
					u.setPin(rs.getString("pin"));
										
					return u;
				});
		return usuario;
	}
	
}
