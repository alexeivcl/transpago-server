package com.jtc.transpagoserver.repository;

import java.util.List;

import com.jtc.transpagoserver.bean.Movimiento;
import com.jtc.transpagoserver.bean.Transferencia;

public interface MovimientoRepository {

	List<Movimiento> obtenerMovimientos(String nroDoc);
	
	Movimiento obtenerMovimiento(Integer id);
	
	Movimiento agregarMovimiento(Movimiento movimiento);
	
	boolean eliminarMovimiento(Integer id);
	
	boolean actualizarMovimiento(Movimiento movimiento, Integer id);

	List<Movimiento> obtenerMovimientosFiltros(String nroDoc, String nroCuenta, String mes, String anno);

	List<String> obtenerMovimientosAnnos();

	Boolean agregarTransferencia(Transferencia transferencia);
	

}
