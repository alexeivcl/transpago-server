package com.jtc.transpagoserver.repository;

import java.util.List;

import com.jtc.transpagoserver.bean.Usuario;

public interface UsuarioRepository {

	List<Usuario> obtenerUsuarios();
	
	Usuario obtenerUsuario(Integer id);
	
	Usuario agregarUsuario(Usuario usuario);
	
	boolean eliminarUsuario(Integer id);
	
	boolean actualizarUsuario(Usuario usuario, Integer id);
	
	Boolean login(Usuario usuario);

}
