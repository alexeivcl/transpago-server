package com.jtc.transpagoserver.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.jtc.transpagoserver.bean.Cuenta;
import com.jtc.transpagoserver.bean.Movimiento;
import com.jtc.transpagoserver.bean.Transferencia;

@SuppressWarnings("unused")
@Repository
public class MovimientoRepositoryImpl implements MovimientoRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CuentaRepository cuentaRepository;

	@Override
	public List<Movimiento> obtenerMovimientos(String nroDoc) {
		

		List<Movimiento> movimientos = jdbcTemplate.query("select * from movimientos m"
				+ " join cuentas c on c.idcuenta = m.idcuenta"
				+ " join usuarios_cuentas uc on uc.idcuenta = c.idcuenta"
				+ " join usuarios u on u.idusuario = uc.idusuario"
				+ " where u.nrodocumento = ?", 
				new Object[] {nroDoc}, 
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento c = new Movimiento();
						c.setId(rs.getInt("idmovimiento"));
						c.setFecha(rs.getDate("fechahora"));						
						c.setIdCuenta(rs.getInt("idcuenta"));
						c.setTipoMov(rs.getString("tipomovimiento"));
						c.setMonto(rs.getInt("monto"));
						list.add(c);
					}
					return list;
				});
		return movimientos;
	}

	@Override
	public Movimiento obtenerMovimiento(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Movimiento agregarMovimiento(Movimiento movimiento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminarMovimiento(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizarMovimiento(Movimiento movimiento, Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Movimiento> obtenerMovimientosFiltros(String nroDoc, String nroCuenta, String mes, String anno) {

		String sql;
		if (nroCuenta.equals("0")) {
			sql = "select * from movimientos m"
					+ " join cuentas c on c.idcuenta = m.idcuenta"
					+ " join usuarios_cuentas uc on uc.idcuenta = c.idcuenta"
					+ " join usuarios u on u.idusuario = uc.idusuario"
					+ " where u.nrodocumento = ? and MONTH(m.fechahora) = ? and YEAR(m.fechahora) = ?";
		}else{
			sql = "select * from movimientos m"
					+ " join cuentas c on c.idcuenta = m.idcuenta"
					+ " join usuarios_cuentas uc on uc.idcuenta = c.idcuenta"
					+ " join usuarios u on u.idusuario = uc.idusuario"
					+ " where u.nrodocumento = ? and c.nrocuenta = ? and MONTH(m.fechahora) = ? and YEAR(m.fechahora) = ?";
		}
		
		List<Movimiento> movimientos = jdbcTemplate.query(sql, 
				(nroCuenta.equals("0")) ? new Object[] {nroDoc, mes, anno} : new Object[] {nroDoc, nroCuenta, mes, anno}, 
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento c = new Movimiento();
						c.setId(rs.getInt("idmovimiento"));
						c.setFecha(rs.getDate("fechahora"));						
						c.setIdCuenta(rs.getInt("idcuenta"));
						c.setTipoMov(rs.getString("tipomovimiento"));
						c.setMonto(rs.getInt("monto"));
						list.add(c);
					}
					return list;
				});
		return movimientos;
	}
	
	@Override
	public List<String> obtenerMovimientosAnnos() {
		
		List<String> annos = jdbcTemplate.query("select distinct YEAR(m.fechahora) as mes from movimientos m", 
				 
				(rs) -> {
					List<String> list = new ArrayList<>();
					while(rs.next()){
						String c = rs.getString("mes");
						list.add(c);
					}
					return list;
				});
		return annos;
	}
	
	@Override
	public Boolean agregarTransferencia(Transferencia transferencia) {
//		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		Integer cred = insertarMovimientoCredito(transferencia.getCuentaDestino(), transferencia.getMonto());
		Integer deb = insertarMovimientoDebito(transferencia.getCuentaOrigen(), transferencia.getMonto());
		if (cred == 1 &&  deb == 1) {
			actualizarSaldoCuentaCredito(transferencia.getCuentaOrigen(), transferencia.getMonto(), 1);
			actualizarSaldoCuentaCredito(transferencia.getCuentaDestino(), transferencia.getMonto(), 0);
			return true;
		}else{
			return false;
		}
		
		
		
//		PreparedStatementCreator psc = (conn) -> {
//			PreparedStatement ps = conn.prepareStatement("insert into empleados values (?,?,?,?)", 
//					Statement.RETURN_GENERATED_KEYS);
//			
//			ps.setNull(1, Types.INTEGER);
////			ps.setString(2, empleado.getNombre());
////			ps.setInt(3,  empleado.getEdad());
////			ps.setLong(4, empleado.getSalario());
//	        return ps;
//		}; 
//		
//		int result = jdbcTemplate.update(psc, keyHolder);
//		
////		if (result > 0)
////			null
//			//transferencia.setId(keyHolder.getKey().intValue());
//		
//		return transferencia;
		
	}
	
	private Integer insertarMovimientoCredito(String cuentaDestino, Integer monto){
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		Cuenta cuentaDest = cuentaRepository.obtenerCuenta(cuentaDestino);
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into movimientos values (?,now(),?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setInt(2, cuentaDest.getId());
			ps.setString(3,  "C");
			ps.setInt(4, monto);
	        return ps;
		}; 
		
		return jdbcTemplate.update(psc, keyHolder);
	}
	
	private Integer insertarMovimientoDebito(String cuentaOrigen, Integer monto){
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		Cuenta cuentaOrig = cuentaRepository.obtenerCuenta(cuentaOrigen);
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into movimientos values (?,now(),?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setInt(2, cuentaOrig.getId());
			ps.setString(3,  "D");
			ps.setInt(4, monto);
	        return ps;
		}; 
		
		return jdbcTemplate.update(psc, keyHolder);
	}
	
	private boolean actualizarSaldoCuentaCredito(String nroCuenta, Integer monto, Integer tipoMov) {
		
		String sql;
		
		if (tipoMov == 1) {
			sql = "update cuentas set saldo = (saldo - ?) where nrocuenta = ?";
		}else{
			sql = "update cuentas set saldo = (saldo + ?) where nrocuenta = ?";
		}
		
		int result = jdbcTemplate.update(sql, 
				monto, nroCuenta);
		return (result > 0) ? true : false;
	}

	

}
