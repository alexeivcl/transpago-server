package com.jtc.transpagoserver.repository;

import java.util.List;

import com.jtc.transpagoserver.bean.Cuenta;
import com.jtc.transpagoserver.bean.Usuario;

public interface CuentaRepository {

	List<Cuenta> obtenerCuentasUsuario(String nroDoc);

	Cuenta obtenerCuenta(String nroCuenta);

	Usuario obtenerUsuarioCuenta(String nroCuenta);

	
}
