package com.jtc.transpagoserver.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jtc.transpagoserver.bean.Usuario;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Usuario> obtenerUsuarios() {

		List<Usuario> usuarios = jdbcTemplate.query("SELECT * from usuarios", 
				(rs) -> {
					List<Usuario> list = new ArrayList<>();
					while(rs.next()){
						Usuario u = new Usuario();
						u.setId(rs.getInt("idusuario"));
						u.setNroDoc(rs.getString("nrodocumento"));
						u.setNombre(rs.getString("nombre"));
						u.setPin(rs.getString("pin"));						
						list.add(u);
					}
					return list;
				});
		return usuarios;
	}

	@Override
	public Usuario obtenerUsuario(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario agregarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminarUsuario(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizarUsuario(Usuario usuario, Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Boolean login(Usuario usuario) {
		
//		Object[] parameters = new Object[] {usuario.getNroDoc(), usuario.getPin()};
//	    Usuario user = jdbcTemplate.queryForObject("select * from usuarios where nrodocumento = ? and pin = ?",
//	        parameters, Usuario.class);
//	    
//		if (user != null) {
//			return true;
//		}
//		return null;
		
		Usuario user = jdbcTemplate.queryForObject("select * from usuarios where nrodocumento = ?", 
				new Object[] {usuario.getNroDoc()}, 
				(rs, rowNum) -> {
					Usuario u = new Usuario();
					u.setId(rs.getInt("idusuario"));
					u.setNombre(rs.getString("nombre"));
					u.setNroDoc(rs.getString("nrodocumento"));
					u.setPin(rs.getString("pin"));
					return u;
		        });
		
		if (user != null) {
			if (user.getPin().equals(usuario.getPin())) {
				return true;
			}
		}
		
		return false;
	}
	
	
	
	
}
