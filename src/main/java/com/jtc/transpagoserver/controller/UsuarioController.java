package com.jtc.transpagoserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtc.transpagoserver.bean.Usuario;
import com.jtc.transpagoserver.repository.UsuarioRepository;

@RestController
@RequestMapping("/rest/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
	public List<Usuario> obtenerUsuarios(){
		
		List<Usuario> usuarios = usuarioRepository.obtenerUsuarios();
		return usuarios;
	}
	
	@PostMapping("/login")
	public Boolean login(@RequestBody Usuario usuario, UriComponentsBuilder uBuilder){	
		
		if (usuarioRepository.login(usuario)) {
			return true;
		}
		return false;
	}
	
//	@PostMapping
//	public ResponseEntity<Empleado> guardarEmpleado(@RequestBody Empleado empleado, UriComponentsBuilder uBuilder) {
//		Empleado c = empleadoRepository.agregarEmpleado(empleado);
//		HttpHeaders header = new HttpHeaders();
//		
//		URI uri = uBuilder.path("/empleados/" )
//				.path(String.valueOf(empleado.getId()))
//				.build()
//				.toUri();
//		
//		header.setLocation(uri);
//		
//		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
//	}
	
}
