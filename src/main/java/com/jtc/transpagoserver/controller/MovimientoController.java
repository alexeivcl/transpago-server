package com.jtc.transpagoserver.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtc.transpagoserver.bean.Movimiento;
import com.jtc.transpagoserver.bean.Transferencia;
import com.jtc.transpagoserver.repository.MovimientoRepository;

@RestController
@RequestMapping("/rest/movimientos")
public class MovimientoController {
	
	@Autowired
	private MovimientoRepository movimientoRepository;

	@GetMapping("/{nroDoc}")
	public List<Movimiento> obtenerMovimientos(@PathVariable("nroDoc") String nroDoc){
		
		List<Movimiento> movimientos = movimientoRepository.obtenerMovimientos(nroDoc); 
		
		return movimientos;
	}
	
	@GetMapping("/{nroDoc}/c/{nroCuenta}/m/{mes}/a/{anno}")
	public List<Movimiento> obtenerMovimientosFiltros(@PathVariable("nroDoc") String nroDoc, @PathVariable("nroCuenta") String nroCuenta, @PathVariable("mes") String mes, @PathVariable("anno") String anno){
		
		List<Movimiento> movimientos = movimientoRepository.obtenerMovimientosFiltros(nroDoc, nroCuenta, mes, anno); 
		
		return movimientos;
	}
	
	
	@GetMapping("/annosFilter")
	public List<String> obtenerAnnosFiltros(){
		
		List<String> annos = movimientoRepository.obtenerMovimientosAnnos(); 
		
		return annos;
	}
	
	@GetMapping("/montoValido/mnt/{monto}/c/{nroCuenta}")
	public Boolean validarMonto(@PathVariable("monto") Integer monto, @PathVariable("nroCuenta") String nroCuenta){
		
		List<String> annos = movimientoRepository.obtenerMovimientosAnnos(); 
		
		return false;
	}
	
	@PostMapping("/agregar")
	public Boolean guardarEmpleado(@RequestBody Transferencia transferencia) {
		
		return movimientoRepository.agregarTransferencia(transferencia);
		
		
		
		
	}

}
