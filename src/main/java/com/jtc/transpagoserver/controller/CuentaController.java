package com.jtc.transpagoserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jtc.transpagoserver.bean.Cuenta;
import com.jtc.transpagoserver.bean.Usuario;
import com.jtc.transpagoserver.repository.CuentaRepository;

@RestController
@RequestMapping("/rest/cuentas")
public class CuentaController {	
	
	@Autowired
	private CuentaRepository cuentaRepository;
	
	@GetMapping("/{nroDoc}")
	public List<Cuenta> cuentasUsuario(@PathVariable("nroDoc") String nroDoc){
		
		List<Cuenta> cuentas = cuentaRepository.obtenerCuentasUsuario(nroDoc); 
		
		return cuentas;
	}
	
	@GetMapping("/c/{nroCuenta}")
	public Cuenta obtenerCuenta(@PathVariable("nroCuenta") String nroCuenta){
		
		Cuenta cuenta = cuentaRepository.obtenerCuenta(nroCuenta); 
		
		return cuenta;
	}
	
	@GetMapping("/uc/{nroCuenta}")
	public Usuario obtenerUsuarioCuenta(@PathVariable("nroCuenta") String nroCuenta){
		
		Usuario usuario = cuentaRepository.obtenerUsuarioCuenta(nroCuenta); 
		
		return usuario;
	}
}
