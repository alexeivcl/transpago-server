package com.jtc.transpagoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TranspagoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TranspagoServerApplication.class, args);
	}
}
