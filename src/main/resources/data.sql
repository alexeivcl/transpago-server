insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (1, '147258', 'Bruce Wayne', '81dc9bdb52d04dc20036dbd8313ed055');

insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (2, '123456', 'Tony Stark', '81dc9bdb52d04dc20036dbd8313ed055');

insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (3, '654321', 'Jhon Connor', '81dc9bdb52d04dc20036dbd8313ed055');

insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (1, '123456789', 'CUENTA CORRIENTE', 'Gs.', '1500000');

insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (2, '298765432', 'CAJA DE AHORRO', 'Gs.', '8500000');

insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (3, '187567522', 'CUENTA CORRIENTE', 'Gs.', '3500000');

insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (4, '193527683', 'CUENTA CORRIENTE', 'Gs.', '800000');

insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (1, 1, 1);

insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (2, 1, 2);

insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (3, 2, 3);

insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (4, 3, 4);

insert into movimientos (idmovimiento, fechahora, idcuenta, tipomovimiento, monto)
values (1, now(), 1, 'D', '50000');

insert into movimientos (idmovimiento, fechahora, idcuenta, tipomovimiento, monto)
values (2, now(), 3, 'C', '50000');